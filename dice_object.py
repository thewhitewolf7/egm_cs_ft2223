import numpy as np
from typing import Union



class Dice:
    def __init__(self, is_fair: bool = True,
                 biased_numbers: Union[int, list] = None,
                 bias_probs: Union[float, list] = None,
                 prob_accuracy: int = None):
        self.is_fair = is_fair
        if biased_numbers or bias_probs:
            self.accuracy = prob_accuracy or 4
            self.is_fair = False
            self.is_rigged = True
            if isinstance(biased_numbers, int) or isinstance(bias_probs, float):
                self.biased_numbers = [biased_numbers]
                self.bias_probs = [bias_probs]
            elif max(biased_numbers) > 6:
                print("Maximum number of spots is six.")
            elif sum(bias_probs) > 1:
                print("Total probability must not exceed one.")
            else:
                self.biased_numbers = biased_numbers
                self.bias_probs = bias_probs
            if len(self.bias_probs) != len(self.biased_numbers):
                print("Number of biased numbers and bias probabilities must match.")
            else:
                self.bias_params = {k[0]: k[1] for k in zip(self.biased_numbers,
                                                            self.bias_probs)}

    @staticmethod
    def roll_once(lower_limit: int = 1, upper_limit: int = 7) -> int:
        return np.random.randint(lower_limit, upper_limit)

    def roll(self, number_of_rolls: int = 1) -> list:
        if self.is_fair:
            die_rolls = []
            for _ in range(number_of_rolls):
                die_rolls.append(self.roll_once())
            return die_rolls
        prob_ranges = self._map_prob_to_range()
        die_rolls = []
        for _ in range(number_of_rolls):
            rolled_number = self.roll_once(1, upper_limit=1 + 10**self.accuracy)
            for num, prob_range in prob_ranges.items():
                if prob_range[0] <= rolled_number < prob_range[1]:
                    die_rolls.append(num)
        return die_rolls

    def _complete_bias_params(self) -> None:
        number_probs = len(self.bias_probs)
        total_sum = sum(self.bias_probs)
        if number_probs < 6:
            remaining_numbers = self._get_remaining_numbers()
            remaining_prob = (1 - total_sum) / len(remaining_numbers)
            for number in remaining_numbers:
                self.bias_params[number] = remaining_prob

    def _get_remaining_numbers(self) -> list:
        remaining_numbers = []
        for number in range(1, 7):
            if number not in self.biased_numbers:
                remaining_numbers.append(number)
        return remaining_numbers

    def _map_prob_to_range(self) -> dict:
        self._complete_bias_params()
        range_per_number = dict()
        lower_limit = 1
        for number, prob in self.bias_params.items():
            number_range = round(prob, self.accuracy) * 10 ** self.accuracy
            upper_limit = lower_limit + number_range
            range_per_number[number] = (lower_limit, upper_limit)
            lower_limit = upper_limit
        return range_per_number
