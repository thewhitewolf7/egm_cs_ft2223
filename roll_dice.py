from typing import Union

import numpy as np
import argparse
import ast

from dice_object import Dice


def calc_freq_alt(counts, number):
    freq = {
        1: 0,
        2: 0,
        3: 0,
        4: 0,
        5: 0,
        6: 0
    }

    for key, value in counts.items():
        freq[key] = round(value / number, 3)

    return freq


def count_numbers(dice_rolls):
    total_counts = {
        1: 0,
        2: 0,
        3: 0,
        4: 0,
        5: 0,
        6: 0
    }

    for number in dice_rolls:
        total_counts[number] = total_counts[number] + 1

    return total_counts


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("biased_numbers", type=ast.literal_eval)
    parser.add_argument("bias_probs", type=ast.literal_eval)
    parser.add_argument("number_of_rolls", type=int)
    args = parser.parse_args()

    number_of_rolls = args.number_of_rolls

    # die = Dice(biased_numbers=[2, 3], bias_probs=[0.2, 0.5])
    # die = Dice(biased_numbers=5, bias_probs=0.6)

    die = Dice(biased_numbers=args.biased_numbers, bias_probs=args.bias_probs)
    dice_rolls = die.roll(number_of_rolls)

    # dice_rolls = np.random.choice([1, 2, 3, 4, 5, 6], size=number_of_rolls,
    #                               p=[0.075, 0.2, 0.5, 0.075, 0.075, 0.075])

    # print(np.unique(dice_rolls, return_counts=True)[0])

    counts = count_numbers(dice_rolls)
    print(counts)
    freq = calc_freq_alt(counts, number_of_rolls)
    print(freq)



    # print(np.round(np.unique(dice_rolls, return_counts=True)[1] /
    #                number_of_rolls, 3))
